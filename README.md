# Test Task

1. Сreate spring boot application.
2. Create endpoint to add a domain to the Mysql Database.
3. Before adding please check the domain for the right HTTP Status code: 502. If the HTTP Status code is right please send a message to Slack channel.
4. Create a simple bearer token authorization checker and use it on API call.
5. Create a simple daily task to check if a application is up and running.
6. Add unit tests.


## About App

If necessary I can show how app works. I used Mysql DB. Created endpoint with POST method for adding to DB. After adding There is SlackService, which sending message to Slack channel, used incoming-webhook. I didn't hide webhook url and db credentials in environmental constant. It's all created for the test task.
```
"/api/domains"
```
After adding There is SlackService, which sending message to Slack channel, used incoming-webhook. I didn't hide webhook url and db credentials in environmental constant. It's all created for the test task.

There are two endpoins, one for registration new user and second one for authentication. It is possible to get bearer token from them. 
```
"/api/auth/register"
"/api/auth/authenticate"
```
I used token for authentication in Postman to add a domain into DB.

Last one is endpoint for check health. It returs Status 200.
```
"/health-check"
```





## About daily task

Maybe I got something mixed up. I just created script for windows (with PowerShell), which is looking for check-health endpoint. And two batch files, first one is showing result in console and second one - adding this action to Task Scheduler.