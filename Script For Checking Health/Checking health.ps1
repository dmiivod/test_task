
$localTime = Get-Date
$localTime.ToString("yyyy-MM-dd HH:mm:ss")

$uri = "http://localhost:8080/health-check"  

try {

    $response = Invoke-WebRequest -Uri $uri 
    $statusCode = [int]$response.StatusCode    

 
    if ($statusCode -eq 200) {
        $OUT = "$($localTime.ToString("yyyy-MM-dd HH:mm:ss")) Server is up and running. Health check passed."
    } else {
        $OUT = "$($localTime.ToString("yyyy-MM-dd HH:mm:ss")) Server is not responding as usual. Status code: $($statusCode)"
    }
} catch {
    $OUT = "$($localTime.ToString("yyyy-MM-dd HH:mm:ss")) Failed to reach the server."
}
$OUT | Out-File -FilePath "$env:USERPROFILE\Desktop\Output.txt"   

