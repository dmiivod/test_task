package com.ivanovdy.testtask.controller;

import com.ivanovdy.testtask.entity.Domain;
import com.ivanovdy.testtask.service.implementation.DomainServiceImpl;
import com.ivanovdy.testtask.service.implementation.SlackServiceImpl;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DomainController {

    DomainServiceImpl domainService;
    SlackServiceImpl slackService;

    @PostMapping("/domains")
    public ResponseEntity<String> saveDomain(@RequestBody Domain domain) {
        int statusCode = domainService.save(domain);

        slackService.sendMessageToSlack("Successfully added " + domain.getUrl()
                + " with status code " + statusCode);

        return ResponseEntity.created(null).build();
    }
}
