package com.ivanovdy.testtask.controller;

import jakarta.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

    @GetMapping("/health-check")
    public void checkHealth(HttpServletResponse response) {
        response.setStatus(200);
    }
}
