package com.ivanovdy.testtask.exception;




public class WrongDomainNameException extends RuntimeException {

    public WrongDomainNameException() {
        super();
    }

    public WrongDomainNameException(String message) {
        super(message);
    }

    public WrongDomainNameException(String message, Throwable cause) {
        super(message, cause);
    }


}
