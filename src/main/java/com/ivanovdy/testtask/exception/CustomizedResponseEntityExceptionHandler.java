package com.ivanovdy.testtask.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {

        ErrorDetailsException errorDetailsException = new ErrorDetailsException(LocalDateTime.now(), ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(errorDetailsException, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(DomainAlreadyExistsException.class)
    public final ResponseEntity<Object> handleDomainAlreadyExistsException(Exception ex, WebRequest request) {

        ErrorDetailsException errorDetailsException = new ErrorDetailsException(LocalDateTime.now(), ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(errorDetailsException, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @ExceptionHandler(WrongDomainNameException.class)
    public final ResponseEntity<Object> handleWrongDomainNameException(Exception ex, WebRequest request) {

        ErrorDetailsException errorDetailsException = new ErrorDetailsException(LocalDateTime.now(), ex.getMessage(),
                request.getDescription(false));

        return new ResponseEntity<>(errorDetailsException, HttpStatus.INTERNAL_SERVER_ERROR);

    }


}
