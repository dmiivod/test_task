package com.ivanovdy.testtask.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class ErrorDetailsException {
    private LocalDateTime timestamp;
    private String message;
    private String details;


}
