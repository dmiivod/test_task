package com.ivanovdy.testtask.exception;

public class DomainAlreadyExistsException extends RuntimeException {

    public DomainAlreadyExistsException() {
        super();
    }

    public DomainAlreadyExistsException(String message) {
        super(message);
    }

    public DomainAlreadyExistsException(String message, Throwable cause) {
        super(message, cause);
    }
}
