package com.ivanovdy.testtask.service.implementation;

import com.ivanovdy.testtask.entity.Domain;
import com.ivanovdy.testtask.exception.DomainAlreadyExistsException;
import com.ivanovdy.testtask.exception.WrongDomainNameException;
import com.ivanovdy.testtask.repository.DomainRepository;
import com.ivanovdy.testtask.service.DomainService;
import jakarta.transaction.Transactional;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class DomainServiceImpl implements DomainService {

    DomainRepository domainRepository;

    @Override
    @Transactional
    public int save(Domain domain) {

        int statusCode = checkDomainStatus(domain);

        if (domainRepository.existsByUrlIgnoreCase(domain.getUrl())) {
            throw new DomainAlreadyExistsException("Already exists in DB");
        }

        if (statusCode != -1) {
            domainRepository.save(domain);
            return statusCode;
        } else {
            throw new WrongDomainNameException("Domain is unreachable");
        }
    }


    public int checkDomainStatus(Domain domain) {
        try {

            URL url = new URL(domain.getUrl());


            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            int statusCode = connection.getResponseCode();

            connection.disconnect();
            if ((statusCode >= 200 && statusCode < 300) || statusCode == 502) {
                return statusCode;
            } else {
                return -1;
            }

        } catch (IOException e) {
            return -1;
        }

    }
}
