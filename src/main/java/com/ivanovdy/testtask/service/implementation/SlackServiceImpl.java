package com.ivanovdy.testtask.service.implementation;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.model.Attachment;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import com.ivanovdy.testtask.service.SlackService;
import lombok.AccessLevel;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.util.Collections;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class SlackServiceImpl implements SlackService {

    static String NEW_LINE = "\n";

    String WEBHOOK = System.getenv("slack.webhook");

    public void sendMessageToSlack(String message) {

        StringBuilder messageBuider = new StringBuilder();
        messageBuider.append(message + NEW_LINE);
        process(messageBuider.toString());
    }

    private void process(String message) {

        Payload payload = Payload.builder()
                .attachments(Collections.singletonList(Attachment.builder()
                        .channelName("#user-created")
                        .build()))
                .text(message).build();
        try {
            WebhookResponse webhookResponse = Slack.getInstance().send(WEBHOOK, payload);

        } catch (IOException e) {
            System.out.println("Something wrong");

        }
    }

}
