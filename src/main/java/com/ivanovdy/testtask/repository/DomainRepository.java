package com.ivanovdy.testtask.repository;

import com.ivanovdy.testtask.entity.Domain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DomainRepository extends JpaRepository<Domain, Long> {

    boolean existsByUrlIgnoreCase(String url);
}
